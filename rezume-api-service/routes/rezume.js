const express = require('express');
const router = express.Router();
const faker = require('faker');
const chance = new require('chance').Chance();
const jsf = require('json-schema-faker');
jsf.extend('chance', () => chance);
jsf.extend('faker', () => faker);

var schema = {
  "type": "array",
  "minItems": 1,
  "maxItems": 1,
  "items": {
  type: 'object',
  properties: {
  name: {
  type: 'string',
  faker: 'name.firstName'
  },
  lastname: {
  type: 'string',
  faker: 'name.lastName'
  },
  status: {
  
  "type": "string",
  "pattern": "online|offline"
  
  },
  address: {
  type: 'string',
  chance: 'address'
  },
  email: {
  "type": "string",
  "chance": {
  "email": {
  "domain": "gmail.com"
  }
  }
  },
  time: {
  
  type: 'integer',
  chance: 'second',
  
  },
  paragraph: {
  type: 'string',
  chance: 'paragraph',
  
  },
  sentence: {
  type: 'string',
  chance: 'sentence'
  },
  company: {
  type: 'company',
  chance: 'company'
  },
  image: {
  type: "string",
  faker: "image.avatar"
  }
  
  
  },
  required: ['status', 'name','lastname', 'address', 'email', 'company', 'image', 'paragraph', 'sentence']
  }
  
  
  };





/* GET users listing. */
router.get('/', (req, res) => {
   jsf.resolve(schema).then(sample =>{
    res.send(sample);
   });



  
});
/* {"name": faker.name.firstName(), 
    "status": faker.random.boolean(),
     "age": chance.age({type: 'teen'}),
    "time": chance.second()
    },
    {"name": faker.name.firstName(), 
    "status": faker.random.boolean(),
     "age": chance.age({type: 'teen'}),
    "time": chance.second()
    },*/
module.exports = router;
