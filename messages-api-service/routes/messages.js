const express = require('express');
const router = express.Router();
const faker = require('faker');
const chance = new require('chance').Chance();
const jsf = require('json-schema-faker');
jsf.extend('chance', () => chance);
jsf.extend('faker', () => faker);


var schema = {
"type": "array",
"minItems": 1,
"maxItems": 3,
"items": {
type: 'object',
properties: {
name: {
type: 'string',
faker: 'name.firstName'
},
lastname: {
type: 'string',
faker: 'name.lastName'
},
status: {

"type": "string",
"pattern": "online|offline"

},


sentence: {
type: 'string',
chance: 'sentence'
},

image: {
type: "string",
faker: "image.avatar"
}

},
required: ['status', 'name','lastname', 'image', 'sentence']
}


};



/* GET users listing. */
router.get('/', (req, res) => {

	jsf.resolve(schema).then(sample => {
	  res.send(sample);
	});
});

module.exports = router;
